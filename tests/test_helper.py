import pytest

from asyncio import Future
from unittest.mock import AsyncMock


def pg_conn_mocker(mocker, location):
    mock_conn_context = MockAsyncContext()
    mock_pg = mocker.patch(location)
    mock_pg.acquire.return_value = mock_conn_context
    return mock_conn_context.mock


def httpx_client_moker(mocker, location):
    mock_httpx_client = MockAsyncContext()
    mock_httpx = mocker.patch(location)
    mock_httpx.AsyncClient.return_value = mock_httpx_client
    return mock_httpx_client.mock


class MockAsyncContext:
    def __init__(self):
        self.mock = AsyncMock()

    async def __aenter__(self):
        return self.mock

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        pass
