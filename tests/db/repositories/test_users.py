from unittest.mock import MagicMock

import pytest
from uuid import uuid4
from asyncpg.exceptions import UniqueViolationError

from sso.db.repositories import users
from sso.service.models.email import Email
from sso.service.exceptions import DuplicateDB
from sso.models.user import User, NewUser
from tests.test_helper import pg_conn_mocker


@pytest.fixture
def mock_pg_conn(mocker):
    pg_location = "sso.db.repositories.users.postgres"
    return pg_conn_mocker(mocker, pg_location)


@pytest.mark.asyncio
async def test_insert(mock_pg_conn):
    # given
    sql = f"INSERT INTO {users.table_name} ({users._col_email}, {users._col_hash}) VALUES($1, $2) RETURNING *"
    uuid = uuid4()
    email = Email("nobody@nowhere.com")
    new = NewUser(email, b"HASH")
    record = {"id": 1, "uuid": uuid, "email": str(email), "hash": b"HASH"}
    expected = User(email, b"HASH", 1, uuid)

    # mock
    mock_pg_conn.fetchrow.return_value = record

    # when
    result = await users.insert(new)

    # then
    mock_pg_conn.fetchrow.assert_called_once_with(sql, str(email), b"HASH")
    assert result == expected


@pytest.mark.asyncio
async def test_insert_duplicate(mock_pg_conn):
    # given
    sql = f"INSERT INTO {users.table_name} ({users._col_email}, {users._col_hash}) VALUES($1, $2) RETURNING *"
    uuid = uuid4()
    email = Email("nobody@nowhere.com")
    new = NewUser(email, b"HASH")
    mock_exception = "MOCK EXCEPTION"

    # mock
    mock_pg_conn.fetchrow.side_effect = UniqueViolationError(mock_exception)

    # when
    with pytest.raises(DuplicateDB) as excinfo:
        result = await users.insert(new)

        # then
        mock_pg_conn.fetchrow.assert_called_once_with(sql, str(email), b"HASH")
        assert str(excinfo.value) == mock_exception


@pytest.mark.asyncio
async def test_find_by_email(mock_pg_conn):
    # given
    sql = f"SELECT * from {users.table_name} WHERE {users._col_email}=$1 LIMIT 1"
    uuid = uuid4()
    email = Email("nobody@nowhere.com")
    record = {"id": 1, "uuid": uuid, "email": str(email), "hash": b"HASH"}
    expected = User(email, b"HASH", 1, uuid)

    # mock
    mock_pg_conn.fetchrow.return_value = record

    # when
    result = await users.find_by_email(email)

    # then
    mock_pg_conn.fetchrow.assert_called_once_with(sql, str(email))
    assert result == expected


@pytest.mark.asyncio
async def test_find_by_email_None(mock_pg_conn):
    # given
    sql = f"SELECT * from {users.table_name} WHERE {users._col_email}=$1 LIMIT 1"
    uuid = uuid4()
    email = Email("nobody@nowhere.com")

    # mock
    mock_pg_conn.fetchrow.return_value = None

    # when
    result = await users.find_by_email(email)

    # then
    mock_pg_conn.fetchrow.assert_called_once_with(sql, str(email))
    assert result == None
