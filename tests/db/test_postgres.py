from unittest.mock import AsyncMock, MagicMock

import pytest
from pytest_mock.plugin import MockerFixture

from sso.db.postgres import acquire, close, _pool
from sso.util.globals import POSTGRES_DB, POSTGRES_HOST, POSTGRES_PASSWORD, POSTGRES_USER
from tests.test_helper import MockAsyncContext


@pytest.mark.asyncio
async def test_pool(mocker: MockerFixture):
    # given
    mock_create_pool: AsyncMock = mocker.patch("sso.db.postgres.asyncpg.create_pool", new_callable=AsyncMock)

    expected_instance = "new-instance"
    mock_create_pool.return_value = expected_instance

    # when
    actual_instance = await _pool()

    # then
    expected_config = {
        "host": POSTGRES_HOST,
        "database": POSTGRES_DB,
        "user": POSTGRES_USER,
        "password": POSTGRES_PASSWORD,
    }
    mock_create_pool.assert_called_once_with(**expected_config)
    assert actual_instance == expected_instance


@pytest.mark.asyncio
async def test_acquire(mocker: MockerFixture):
    # given
    mock_pool: AsyncMock = mocker.patch("sso.db.postgres._pool", new_callable=AsyncMock)
    mock_acquire = MagicMock(MockAsyncContext())
    expected_connection = "acquired-connection"
    mock_acquire.return_value.__aenter__.return_value = expected_connection
    mock_pool.return_value.acquire = mock_acquire

    # when
    actual_connection = None
    async with acquire() as c:
        actual_connection = c

    # then
    mock_pool.assert_called_once()
    mock_acquire.assert_called_once()
    assert actual_connection == expected_connection


@pytest.mark.asyncio
async def test_close(mocker: MockerFixture):
    # Test no exception
    # given
    mock_close: AsyncMock = AsyncMock()

    # mocking
    mock_pool: AsyncMock = mocker.patch("sso.db.postgres._pool", new_callable=AsyncMock)
    mock_pool.return_value.close = mock_close

    # when
    await close()

    # then
    mock_pool.assert_awaited_once()
    mock_close.assert_awaited_once()
    mock_pool.reset_mock()
    mock_close.reset_mock()


@pytest.mark.asyncio
async def test_close_exception(mocker: MockerFixture):
    # Test exception
    # given
    mock_exception = "Mock Exception"
    mock_close: AsyncMock = AsyncMock()

    # mocking
    mock_pool: AsyncMock = mocker.patch("sso.db.postgres._pool", new_callable=AsyncMock)
    mock_pool.side_effect = Exception(mock_exception)

    # when
    with pytest.raises(Exception) as excinfo:
        await close()

        # then
        mock_pool.assert_awaited_once()
        mock_close.assert_not_awaited()
        assert str(excinfo.value) == mock_exception
