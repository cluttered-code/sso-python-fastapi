import json

from unittest.mock import AsyncMock
from pytest_mock.plugin import MockerFixture
from uuid import uuid4
from starlette.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from starlette.testclient import TestClient

from sso.util.globals import PUBLIC_KEY
from sso.service.models.jwt import DecodedJWT
from sso.service.exceptions import InvalidAgument, DuplicateDB


def test_api_register(api_client: TestClient, mocker: MockerFixture):
    # given
    email = "nobody@nowhere.com"
    password = "12345678"

    # mocking
    mock_sign_in = mocker.patch("sso.router.v0.auth.auth.register", new_callable=AsyncMock)
    mock_sign_in.return_value = email

    # when
    response = api_client.post("/v0/register", json={"email": email, "password": password})

    # then
    assert response.status_code == HTTP_200_OK
    assert response.json() == email


def test_api_register_bad_request(api_client: TestClient, mocker: MockerFixture):
    # given
    email = "nobody@nowhere.com"
    password = "12345678"

    # mocking
    mock_email = mocker.patch("sso.router.v0.auth.Email")
    mock_email.side_effect = InvalidAgument("email", "bad")

    # when
    response = api_client.post("/v0/register", json={"email": email, "password": password})

    # then
    assert response.status_code == HTTP_400_BAD_REQUEST
    assert response.json() == {"detail": "email: bad"}


def test_api_register_duplicate(api_client: TestClient, mocker: MockerFixture):
    # given
    email = "nobody@nowhere.com"
    password = "12345678"

    # mocking
    mock_sign_in = mocker.patch("sso.router.v0.auth.auth.register", new_callable=AsyncMock)
    mock_sign_in.side_effect = DuplicateDB(Exception("duplicate user"))

    # when
    response = api_client.post("/v0/register", json={"email": email, "password": password})

    # then
    assert response.status_code == HTTP_409_CONFLICT
    assert response.json() == {"detail": f"{email} already registered"}


def test_api_sign_in(api_client: TestClient, mocker: MockerFixture):
    # given
    email = "nobody@nowhere.com"
    password = "12345678"
    expected_jwt = "JWT"

    # mocking
    mock_sign_in = mocker.patch("sso.router.v0.auth.auth.sign_in", new_callable=AsyncMock)
    mock_sign_in.return_value = expected_jwt

    # when
    response = api_client.post("/v0/sign-in", json={"email": email, "password": password})

    # then
    assert response.status_code == HTTP_200_OK
    assert response.json() == expected_jwt


def test_api_sign_in_unauthorized(api_client: TestClient, mocker: MockerFixture):
    # given
    email = "nobody@nowhere.com"
    password = "12345678"

    # mocking
    mock_sign_in = mocker.patch("sso.router.v0.auth.auth.sign_in", new_callable=AsyncMock)
    mock_sign_in.return_value = None

    # when
    response = api_client.post("/v0/sign-in", json={"email": email, "password": password})

    # then
    assert response.status_code == HTTP_401_UNAUTHORIZED
    assert response.json() == {"detail": "unauthorized"}


def test_api_sign_in_bad_request(api_client: TestClient, mocker: MockerFixture):
    # given
    email = "nobody@nowhere.com"
    password = "12345678"

    # mocking
    mock_email = mocker.patch("sso.router.v0.auth.Email")
    mock_email.side_effect = InvalidAgument("email", "bad")

    # when
    response = api_client.post("/v0/sign-in", json={"email": email, "password": password})

    # then
    assert response.status_code == HTTP_401_UNAUTHORIZED
    assert response.json() == {"detail": "unauthorized"}


def test_verify_jwt(api_client: TestClient, mocker: MockerFixture):
    # given
    sub_uuid = uuid4()

    jwt = DecodedJWT(aud="aud", iss="iss", nbf=0, exp=0, sub=str(sub_uuid), iat=0)
    jwt_json = jwt.json()
    jwt_dict = jwt.dict()
    jwt_dict["sub"] = str(jwt_dict["sub"])
    headers = {"test-jwt": jwt_json}

    # when
    response = api_client.get("/v0/verify-token", headers=headers)

    # then
    assert response.status_code == HTTP_200_OK
    assert response.json() == jwt_dict


def test_api_public_key(api_client: TestClient, mocker: MockerFixture):
    # when
    response = api_client.get("/v0/public-key")

    # then
    assert response.status_code == HTTP_200_OK
    assert response.json() == PUBLIC_KEY
