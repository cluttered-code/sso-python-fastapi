from starlette.testclient import TestClient
from starlette.status import HTTP_200_OK


def test_ping(api_client: TestClient):
    response = api_client.get("/ping")

    assert response.status_code == HTTP_200_OK
    assert response.json() == "pong"


def test_root_redirect(api_client: TestClient):
    response = api_client.get("/")

    assert response.status_code == HTTP_200_OK
    assert response.text.__contains__("swagger")
