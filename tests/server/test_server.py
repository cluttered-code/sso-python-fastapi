from copy import deepcopy
from unittest.mock import AsyncMock, ANY

import pytest
from pytest_mock.plugin import MockerFixture

from sso.server import server
from sso.util.globals import VERSION


def test_custom_openapi(mocker: MockerFixture):
    # Test app.openapi_schema
    # given
    mock_app = mocker.patch("sso.server.server.app")
    expected_openapi_schema = "openapi-schema"
    mock_app.openapi_schema = expected_openapi_schema

    # when
    actual_openapi_schema = server.custom_openapi()

    # then
    assert actual_openapi_schema == expected_openapi_schema

    # Test not app.openapi_schema
    # given
    mock_app.openapi_schema = None
    expected_routes = "routes"
    mock_app.routes = expected_routes
    initial_openapi_schema = {"info": {"info-key": "info-value"}}
    final_openapi_schema = deepcopy(initial_openapi_schema)
    final_openapi_schema["info"]["x-logo"] = {"url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"}
    mock_get_openapi = mocker.patch("sso.server.server.get_openapi")
    mock_get_openapi.return_value = initial_openapi_schema

    # when
    actual_openapi_schema = server.custom_openapi()

    # then
    mock_get_openapi.assert_called_once_with(
        title="SSO",
        version=VERSION,
        description=ANY,
        routes=expected_routes,
    )
    assert actual_openapi_schema == final_openapi_schema
    assert mock_app.openapi_schema == final_openapi_schema


@pytest.mark.asyncio
async def test_shutdown(mocker: MockerFixture):
    # given
    server.PY_ENV = "production"
    mock_postgres_close = mocker.patch("sso.server.server.postgres.close", new_callable=AsyncMock)

    # When
    await server.shutdown()

    # then
    mock_postgres_close.assert_awaited_once()
