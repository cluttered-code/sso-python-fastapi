import sys
import os

# In order for imports to work in our tested code, we need to add the root
# package to the path.  Only the root is necessary
root_dir = os.path.dirname(__file__)
api_dir = os.path.join(root_dir, "../sso")
abs_api_dir = os.path.abspath(api_dir)
sys.path.insert(0, abs_api_dir)
