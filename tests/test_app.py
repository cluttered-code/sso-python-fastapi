import os
from unittest.mock import MagicMock
from pytest_mock.plugin import MockerFixture

from sso.app import start_app


def test_start_app(mocker: MockerFixture):
    # given
    uvicorn_mock: MagicMock = mocker.patch("sso.app.uvicorn")

    # when
    start_app()

    # then
    uvicorn_mock.run.assert_called_once_with(
        "sso.server.server:app", host="0.0.0.0", port=50050, server_header=False, reload=False
    )
