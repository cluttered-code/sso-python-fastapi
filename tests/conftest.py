# Fixtures defined in this file are automatically available in all tests
import pytest
from starlette.testclient import TestClient

from sso.server.server import app


@pytest.fixture(scope="session")
def api_client() -> TestClient:
    client = TestClient(app)
    yield client
