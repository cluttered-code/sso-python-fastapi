from fastapi import APIRouter, HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT

from sso.util.globals import PUBLIC_KEY
from sso.models.auth import Credentials
from sso.service import auth
from sso.service.exceptions import InvalidAgument, DuplicateDB
from sso.service.models.email import Email
from sso.service.models.password import Password
from sso.service.models.jwt import DecodedJWT
from sso.middleware.auth import jwt_required

auth_router = APIRouter()


@auth_router.post("/register", response_model=str)
async def api_register(cred: Credentials) -> str:
    try:
        email = Email(cred.email)
        password = Password(cred.password)
        await auth.register(email, password)
        return str(email)
    except InvalidAgument as e:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=f"{e.arg}: {e.cause}") from e
    except DuplicateDB as e:
        raise HTTPException(status_code=HTTP_409_CONFLICT, detail=f"{cred.email} already registered") from e


@auth_router.post("/sign-in", response_model=str)
async def api_sign_in(cred: Credentials) -> str:
    try:
        email = Email(cred.email)
        password = Password(cred.password)
        jwt = await auth.sign_in(email, password)
        if not jwt:
            raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="unauthorized")
        return jwt
    except InvalidAgument as e:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="unauthorized") from e


@auth_router.get("/verify-token")
async def api_verify_jwt(token: DecodedJWT = jwt_required) -> DecodedJWT:
    return token


@auth_router.get("/public-key", response_model=str)
async def api_public_key() -> str:
    return PUBLIC_KEY
