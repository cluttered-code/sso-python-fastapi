from fastapi import APIRouter
from starlette.responses import Response
from starlette.status import HTTP_302_FOUND

root_router = APIRouter()


# endpoint used by kubernetes to determine if service is alive
@root_router.get("/ping", include_in_schema=False)
async def ping():
    return "pong"


# redirect root to /docs for better user experience.
# 302 redirect is temporary so root endpoint could be used in the future without need for UI change
@root_router.get("/", include_in_schema=False)
async def redirect_root(response: Response):
    response.status_code = HTTP_302_FOUND
    response.headers["Location"] = "/docs"
