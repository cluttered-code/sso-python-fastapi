from uuid import UUID
from dataclasses import dataclass

from sso.service.models.email import Email


@dataclass
class NewUser:
    email: Email
    hash: bytes


@dataclass
class User(NewUser):
    id: int
    uuid: UUID

    def __eq__(self, other):
        if isinstance(other, User):
            return (
                self.id == other.id
                and self.uuid == other.uuid
                and self.email == other.email
                and self.hash == other.hash
            )
        return NotImplemented
