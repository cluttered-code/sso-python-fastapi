from pydantic import BaseModel  # pylint: disable=no-name-in-module


class Credentials(BaseModel):
    email: str
    password: bytes
