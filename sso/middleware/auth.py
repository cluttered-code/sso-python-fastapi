import json
import jwt

from fastapi import Depends, HTTPException
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from starlette.requests import Request
from starlette.status import HTTP_401_UNAUTHORIZED

from sso.util.logger import log
from sso.util.globals import PY_ENV
from sso.service.models.jwt import DecodedJWT
from sso.util.jwt import decode


# Provides 401 responses instead of 403
class _HTTPBearerJWT(HTTPBearer):
    async def __call__(self, request: Request) -> DecodedJWT:
        if PY_ENV != "test":
            try:
                token: HTTPAuthorizationCredentials = await super().__call__(request)
                return decode(token.credentials)
            except jwt.ExpiredSignatureError as ex:
                raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="expired token") from ex
            except jwt.InvalidAudienceError as ex:
                raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="invalid audience") from ex
            except jwt.InvalidIssuerError as ex:
                raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="invalid issuer") from ex
            except Exception as ex:
                log.error("invalid token: %s (%s)", ex, token.credentials)
                raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="invalid token") from ex
        else:
            # load raw JWT during testing using 'test-jwt' header
            jwt_header = request.headers.get("test-jwt")
            if not jwt_header:
                raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="missing 'test-jwt' header")
            jwt_dict = json.loads(jwt_header)
            return DecodedJWT(**jwt_dict)


jwt_required = Depends(_HTTPBearerJWT())
