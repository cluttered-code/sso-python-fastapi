import uvicorn

from sso.util.globals import PY_ENV, VERSION
from sso.util.logger import log

log.info(f"######## SSO v:{VERSION} ########")


def start_app():
    app = "sso.server.server:app"
    reload = PY_ENV == "development"

    uvicorn.run(app, host="0.0.0.0", port=50050, server_header=False, reload=reload)


if __name__ == "__main__":
    start_app()
