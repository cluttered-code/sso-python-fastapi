import logging
import sys

from sso.util.globals import PY_ENV

level = logging.INFO
if PY_ENV == "development":
    level = logging.DEBUG

logging.basicConfig(
    stream=sys.stdout, format="[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s", level=level
)


# This can be setup to use a logging service (ie. logstash) per environment (ie. staging, production)
log = logging.getLogger("sso")
