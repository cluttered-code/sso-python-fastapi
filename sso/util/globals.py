import os

PY_ENV = os.getenv("PY_ENV", "development")
AUDIENCE = os.getenv("AUDIENCE", "development")
VERSION = os.getenv("ROLLOUT_VERSION", "development")

PUBLIC_KEY = os.getenv("PUBLIC_KEY", "")
PRIVATE_KEY = os.getenv("PRIVATE_KEY", "")

POSTGRES_HOST = os.getenv("POSTGRES_HOST", "sso-postgres")
POSTGRES_DB = os.getenv("POSTGRES_DB", "sso")
POSTGRES_USER = os.getenv("POSTGRES_USER", "sso_dev")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD", "wordpass")
