import jwt

from datetime import datetime, timezone, timedelta

from sso.models.user import User
from sso.service.models.jwt import DecodedJWT
from sso.util.globals import AUDIENCE, PUBLIC_KEY, PRIVATE_KEY

issuer = "cluttered-code"
algorithm = "RS256"


def sign(user: User) -> str:
    now = datetime.now(tz=timezone.utc)
    expires = now + timedelta(hours=1)

    token = jwt.encode(
        {"sub": str(user.uuid), "iat": now, "nbf": now, "exp": expires, "iss": issuer, "aud": AUDIENCE},
        PRIVATE_KEY,
        algorithm=algorithm,
    )

    return token


def decode(token: bytes) -> DecodedJWT:
    decoded = jwt.decode(token, PUBLIC_KEY, audience=AUDIENCE, issuer=issuer, algorithms=[algorithm])
    return DecodedJWT(**decoded)
