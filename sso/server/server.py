from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi

from sso.util.logger import log
from sso.db import postgres
from sso.util.globals import PY_ENV, VERSION
from sso.middleware.cors import enable_cors
from sso.router.root import root_router
from sso.router.v0.router import v0_router

app = FastAPI()

# CORS
enable_cors(app)

# Routers
app.include_router(v0_router, prefix="/v0")
app.include_router(root_router)


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="SSO",
        version=VERSION,
        description="""Single Sign On using JSON Web Tokens (JWT)
        
        1. /register a user with email and password
        2. /sign-in using the created user
            a. copy the returned JWT
            b. paste the JWT in the the 'Authorize' form
        3. /verify-token to see that the JWT worked properly
        4. /public-key is available so any other microservice could use these JWTs to verify users
        """,
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {"url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"}
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi


@app.on_event("startup")
async def startup():
    log.info("server startup")
    # vscode debugger
    if PY_ENV == "development":
        import ptvsd

        ptvsd.enable_attach(address=("0.0.0.0", 50051))

    if PY_ENV != "test":
        ...


@app.on_event("shutdown")
async def shutdown():
    log.info("server shutdown")
    await postgres.close()
