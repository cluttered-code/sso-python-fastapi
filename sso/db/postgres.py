from contextlib import asynccontextmanager
from typing import AsyncContextManager

import asyncpg
from asyncpg.connection import Connection
from asyncpg.pool import Pool

from sso.util.globals import POSTGRES_DB, POSTGRES_HOST, POSTGRES_PASSWORD, POSTGRES_USER
from sso.util.logger import log


config = {"host": POSTGRES_HOST, "database": POSTGRES_DB, "user": POSTGRES_USER, "password": POSTGRES_PASSWORD}

_instance: Pool = None


async def _pool() -> Pool:
    global _instance  # pylint: disable=global-statement
    if _instance is None:
        _instance = await asyncpg.create_pool(**config)
    return _instance


@asynccontextmanager
async def acquire() -> AsyncContextManager[Connection]:
    p = await _pool()
    async with p.acquire() as c:
        yield c


async def close():
    log.info("closing database...")
    try:
        p = await _pool()
        await p.close()
    except Exception:
        log.critical("CANNOT CLOSE DATABASE")
        raise
