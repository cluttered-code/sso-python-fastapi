from asyncpg import Record
from asyncpg.exceptions import UniqueViolationError

from sso.db import postgres
from sso.service.models.email import Email
from sso.service.exceptions import DuplicateDB
from sso.models.user import NewUser, User

table_name = "users"

_col_id = "id"
_col_uuid = "uuid"
_col_email = "email"
_col_hash = "hash"


async def insert(user: NewUser) -> User:
    sql = f"INSERT INTO {table_name} ({_col_email}, {_col_hash}) VALUES($1, $2) RETURNING *"
    async with postgres.acquire() as con:  # type: Connection
        try:
            record = await con.fetchrow(sql, user.email, user.hash)
            user = _recordToUser(record)
            return user
        except UniqueViolationError as e:
            raise DuplicateDB(e) from e


async def find_by_email(email: Email) -> User | None:
    sql = f"SELECT * from {table_name} WHERE {_col_email}=$1 LIMIT 1"
    async with postgres.acquire() as con:  # type: Connection
        record = await con.fetchrow(sql, email)
        user = _recordToUser(record) if record else None
        return user


def _recordToUser(record: Record) -> User:
    return User(Email(record[_col_email]), record[_col_hash], record[_col_id], record[_col_uuid])
