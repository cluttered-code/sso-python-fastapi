from sso.service.exceptions import InvalidAgument

_min_length = 8


class Password(bytes):
    """
    Password is an immutable 'bytes' obj with formatting and validation guarentees.
    """

    def __new__(cls, password: bytes):
        if not password:
            raise InvalidAgument("password", "missing")

        if len(password) < _min_length:
            raise InvalidAgument("password", f"minimum length {_min_length}")

        return bytes.__new__(cls, password)
