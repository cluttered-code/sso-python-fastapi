import re

from sso.service.exceptions import InvalidAgument

regex = re.compile(r"([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+")


class Email(str):
    """
    Email is an immutable str with formatting and validation guarentees.
    """

    def __new__(cls, email: str):
        if not email:
            raise InvalidAgument("email", "missing")

        formattedEmail = email.lower()
        if not re.fullmatch(regex, formattedEmail):
            raise InvalidAgument("email", "invalid format")

        return str.__new__(cls, formattedEmail)
