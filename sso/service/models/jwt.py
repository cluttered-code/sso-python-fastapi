from uuid import UUID
from pydantic import BaseModel  # pylint: disable=no-name-in-module


class DecodedJWT(BaseModel):
    sub: UUID
    iat: int
    nbf: int
    exp: int
    iss: str
    aud: str
