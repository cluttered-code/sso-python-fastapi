import bcrypt

from sso.util import jwt
from sso.db.repositories import users
from sso.service.models.email import Email
from sso.service.models.password import Password
from sso.models.user import NewUser

# _invalid_hash - used when user doesn't exist for more consistent sign in computation times
_invalid_hash = bcrypt.hashpw(bcrypt.gensalt(), bcrypt.gensalt())


async def register(email: Email, password: Password):
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(password, salt)
    new_user = NewUser(email, hashed)
    await users.insert(new_user)


async def sign_in(email: Email, password: Password) -> str:
    user = await users.find_by_email(email)
    hashed = user.hash if user else _invalid_hash
    valid = bcrypt.checkpw(password, hashed) and user is not None
    if not valid:
        return None

    token = jwt.sign(user)
    return token
