# business level exceptions


class InvalidAgument(Exception):
    arg: str
    cause: str

    def __init__(self, arg: str, cause: str):
        super().__init__(f"invalid argument {arg} because {cause}")
        self.arg = arg
        self.cause = cause


class DuplicateDB(Exception):
    def __init__(self, ex: Exception):
        super().__init__(str(ex))
