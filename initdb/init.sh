#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    CREATE TABLE IF NOT EXISTS users (
        id BIGSERIAL PRIMARY KEY,
        uuid uuid DEFAULT uuid_generate_v4(),
        email text CHECK (email <> '') NOT NULL,
        hash bytea NOT NULL
    );
    CREATE UNIQUE INDEX IF NOT EXISTS users_uuid_idx ON users(uuid);
    CREATE UNIQUE INDEX IF NOT EXISTS users_email_idx ON users(email);
EOSQL