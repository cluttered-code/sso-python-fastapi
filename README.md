# SSO - Python-fastapi

![coverage](https://gitlab.com/cluttered-code/sso-python-fastapi/badges/main/coverage.svg?job=unit-test)
![pipeline](https://gitlab.com/cluttered-code/sso-python-fastapi/badges/main/pipeline.svg)

This project is setup to be run in a docker container so it is easy to deploy to swarm or kubernetes.

It is also setup to be developed using docker compose keeping it as close to production with extra dev features:

- initialized empty postgresql database
- auto code reaload on changes within `/sso` directory for fast manual verification
- auto code formatting and linting on save
- Test Driven Development (TDD) with `ptw`
- preconfigured debugger in vscode
- local DNS for easier usage
- CI/CD pipeline with verfy basic features that can easily be expanded to perform deployments to multiple environments (ie. stage, production) with 1-click safe rollback

## Development

### URLs

- **Server:** [http://sso.localhost](http://sso.localhost)
- **DNS:** [http://traefik.sso.localhost](http://traefik.sso.localhost)
- **Database:** `db.sso.localhost:5432`

## Virtual Environment

`.venv` will automatically be used in vscode terminal.

### Initialize

```bash
# Windows
python -m venv .venv

# Linux
python3 -m venv .venv
```

### Activate

```bash
source .venv/bin/activate
```

### Dependencies

```bash
# Windows
pip install -U pip setuptools wheel
pip install -r requirements/test.txt

# Linux
pip install -U pip setuptools wheel
pip install -r requirements/test.linux.txt
```

## Run

### Docker

```bash
docker compose up -d --build --force-recreate
```

## Test

SSO requires python 3.10 to run tests locally

```bash
# TDD
ptw

# Single
pytest --cov=sso --cov-report=term --cov-report=html
```
