FROM python:3.10-slim as base

ENV PYTHONUNBUFFERED=1\
    PYTHONPATH=/app:${PYTHONPATH}

WORKDIR /app
COPY . /app

RUN apt-get update &&\
    apt-get full-upgrade -y &&\
    apt-get install git gcc -y &&\
    pip install -U pip setuptools wheel &&\
    pip install --no-cache-dir -r requirements/common.linux.txt


#### Development ####
FROM base as development

ENV PY_ENV=development

RUN pip install --no-cache-dir -r requirements/test.linux.txt

WORKDIR /app/sso

EXPOSE 50050 50051

CMD ["python", "app.py"]


#### production ####
FROM base as production

ENV PY_ENV=production

RUN apt-get remove --purge git gcc -y &&\
    apt-get autoremove -y &&\
    apt-get autoclean -y &&\
    rm -rf tests requirements

WORKDIR /app/sso

EXPOSE 50050

CMD ["python", "app.py"]